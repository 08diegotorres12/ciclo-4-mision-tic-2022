const mongoose = require('mongoose');

const modeloCategoria = require('../models/model_categorias');
const modeloProducto = require('../models/model_productos');

const miconexion = require('../conexion')

modeloCategoria.aggregate([{
    $lookup: {
        from: "productos",
        localField: "id",
        foreignField: "id_categoria",
        as: "productos_categoria",
    },
},
{
    $unwind: "$productos_categoria",
}
])
.then((result)=>{console.log(result)})
.catch((error)=>{console.log(error)});


/*
var dataCategorias = [];
modeloCategoria.find({ nombre: "Comestibles" }).then(data => {
        console.log("Categoria:")
        console.log(data);
        data.map((d, k) => {dataCategorias.push(d.id);})
        modeloProducto.find({ id_categoria: { $in: dataCategorias } })
            .then(data => {
                console.log("Productos de la categoria:")
                console.log(data);
            })
            .catch(error => {
                console.log(error);
            })
    })
    .catch(error => {
        console.log(error);
    })
*/

/*
https://www.mongodb.com/docs/manual/core/aggregation-pipeline/
db.orders.aggregate( [
   // Stage 1: Filter pizza order documents by date range
   {
      $match:
      {
         "date": { $gte: new ISODate( "2020-01-30" ), $lt: new ISODate( "2022-01-30" ) }
      }
   },
   // Stage 2: Group remaining documents by date and calculate results
   {
      $group:
      {
         _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
         totalOrderValue: { $sum: { $multiply: [ "$price", "$quantity" ] } },
         averageOrderQuantity: { $avg: "$quantity" }
      }
   },
   // Stage 3: Sort documents by totalOrderValue in descending order
   {
      $sort: { totalOrderValue: -1 }
   }
 ] )
 */