const express = require('express');
const router = express.Router();

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorProductos = require('../controllers/controller_productos');
router.get("/listar",controladorProductos);
router.get("/cargar/:id",controladorProductos);
router.post("/agregar",controladorProductos);
router.post("/editar/:id",controladorProductos);
router.delete("/borrar/:id",controladorProductos);

//CONFIGURACION DE LA RUTA CON EL METODO SUGERIDO EN LA GUIA
const controladorProductosGuia = require('../controllers/controller_productos_guia');
router.get("/listarguia/:id?",controladorProductosGuia.productosListar);
router.post("/agregarguia/:id?",controladorProductosGuia.productosAgregar);
router.post("/editarguia/:id?",controladorProductosGuia.productosEditar);
router.delete("/borrarguia/:id?",controladorProductosGuia.productosBorrar);

//CONFIGURACION DE LA RUTA CON EL METODO LEONARDO
const controladorProductosLeonardo = require('../controllers/controller_productos_leonardo');
router.get("/listarleonardo/:id?",controladorProductosLeonardo.productosListar);
router.get("/cargarleonardo/:id?",controladorProductosLeonardo.productosCargar);
router.post("/agregarleonardo/:id?",controladorProductosLeonardo.productosAgregar);
router.post("/editarleonardo/:id?",controladorProductosLeonardo.productosEditar);
router.delete("/borrarleonardo/:id?",controladorProductosLeonardo.productosBorrar);

module.exports = router
